package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class Hotel(
    @Json(name = "address")
    val address: String, // 40 Lò Sũ, Lý Thái Tổ, Hoàn Kiếm, Hà Nội, Việt Nam
    @Json(name = "city")
    val city: String, // 5ccaec2a6a3a046671ed7126
    @Json(name = "description")
    val description: String, // Hanoi Capella Hotel là chỗ nghỉ mô phỏng ngôi làng châu Âu thế kỷ 19 và có cáp treo. Khách sạn có WiFi miễn phí, tiện nghi BBQ, trung tâm spa, nhà hàng cũng như quán bar.
    @Json(name = "facilities")
    val facilities: List<Facility>,
    @Json(name = "_id")
    val id: String, // 5ccaff3a810143002a62ff14
    @Json(name = "images")
    val images: List<String>,
    @Json(name = "location")
    val location: Location,
    @Json(name = "name")
    val name: String, // Hanoi Capella Hotel
    @Json(name = "num_review")
    val numReview: Int, // 6
    @Json(name = "num_rooms")
    val numRooms: Int, // 1
    @Json(name = "rate")
    val rate: Double, // 3.3333333333333335
    @Json(name = "rooms")
    val rooms: List<Room>,
    @Json(name = "rules")
    val rules: List<Rule>
)