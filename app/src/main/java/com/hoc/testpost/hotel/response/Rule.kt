package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class Rule(
    @Json(name = "content")
    val content: String, // 12:00 pm
    @Json(name = "_id")
    val id: String, // 5ccaff3a810143002a62ff16
    @Json(name = "name")
    val name: String // Check In
)