package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class Meta(
    @Json(name = "page")
    val page: Int, // 1
    @Json(name = "page_size")
    val pageSize: Int, // 2
    @Json(name = "total_page")
    val totalPage: Int // 1
)