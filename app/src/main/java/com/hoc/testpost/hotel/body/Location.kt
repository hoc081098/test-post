package com.hoc.testpost.hotel.body


import com.squareup.moshi.Json

data class Location(
    @Json(name = "address")
    val address: String, // Hồ gươm
    @Json(name = "coordinates")
    val coordinates: List<Double>
)