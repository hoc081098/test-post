package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class Data(
    @Json(name = "hotels")
    val hotels: List<Hotel>
)