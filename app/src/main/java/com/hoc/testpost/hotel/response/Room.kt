package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class Room(
    @Json(name = "adults")
    val adults: Int, // 2
    @Json(name = "amenities")
    val amenities: List<Amenity>,
    @Json(name = "beds")
    val beds: Int, // 2
    @Json(name = "children")
    val children: Int, // 1
    @Json(name = "description")
    val description: String, // Nestled in the heart of Marin County, Best Western PLUS Novato Oaks Inn is an ideal spot from which to discover San Francisco (CA). From here, guests can enjoy easy access to all that the lively city has to offer. With its convenient location, the hotel offers easy access to the city’s must-see destinations.At Best Western PLUS Novato Oaks Inn, every effort is made to make guests feel comfortable. To do so, the hotel provides the best in services and amenities. A selection of top-class facilities such as 24-hour front desk, Wi-Fi in public areas, car park, room service, family room can be enjoyed at the hotel.Guests can choose from 107 rooms, all of which exude an atmosphere of total peace and harmony. The hotel’s hot tub, fitness center, outdoor pool are ideal places to relax and unwind after a busy day. When you are looking for comfortable and convenient accommodations in San Francisco (CA), make Best Western PLUS Novato Oaks Inn your home away from home.
    @Json(name = "hotel")
    val hotel: String, // 5ccaff3a810143002a62ff14
    @Json(name = "_id")
    val id: String, // 5ccaff60810143002a62ff17
    @Json(name = "images")
    val images: List<String>,
    @Json(name = "name")
    val name: String, // Room 201
    @Json(name = "num_review")
    val numReview: Int, // 0
    @Json(name = "price")
    val price: Int, // 100
    @Json(name = "rate")
    val rate: Int, // 0
    @Json(name = "square")
    val square: Int, // 30
    @Json(name = "status")
    val status: String, // AVAILABLE
    @Json(name = "__v")
    val v: Int // 0
)