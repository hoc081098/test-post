package com.hoc.testpost.hotel.response

import com.squareup.moshi.Json

data class Amenity(
    @Json(name = "icon")
    val icon: String, // uploads/ic_placeholder.png
    @Json(name = "_id")
    val id: String, // 5ccaff60810143002a62ff18
    @Json(name = "name")
    val name: String, // Air Conditioning
    @Json(name = "status")
    val status: Boolean // false
)