package com.hoc.testpost.hotel.body

import com.hoc.testpost.hotel.response.Guests
import com.squareup.moshi.Json

data class SearchHotelBody(
    @Json(name = "dates")
    val dates: List<String>,
    @Json(name = "guests")
    val guests: Guests,
    @Json(name = "location")
    val location: Location,
    @Json(name = "prices")
    val prices: List<Int>
)