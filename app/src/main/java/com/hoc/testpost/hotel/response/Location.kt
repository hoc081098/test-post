package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class Location(
    @Json(name = "coordinates")
    val coordinates: List<Double>,
    @Json(name = "type")
    val type: String // Point
)