package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class Guests(
    @Json(name = "peoples")
    val peoples: Int, // 1
    @Json(name = "rooms")
    val rooms: Int // 1
)