package com.hoc.testpost.hotel.response


import com.squareup.moshi.Json

data class SearchHotelReponse(
    @Json(name = "data")
    val `data`: Data,
    @Json(name = "meta")
    val meta: Meta,
    @Json(name = "success")
    val success: Boolean // true
)