package com.hoc.testpost

import com.hoc.testpost.flight.body.SearchFlightBody
import com.hoc.testpost.flight.response.SearchFlightResponse
import com.hoc.testpost.hotel.body.SearchHotelBody
import com.hoc.testpost.hotel.response.SearchHotelReponse
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.create
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

const val BASE_URL = "http://api.didauday.me/"
const val TOKEN =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5nb2NuZ3V5ZW5tbW9AZ21haWwuY29tIiwiX2lkIjoiNWNjZWU1MjMxYmZhYzAwMD" +
            "JhMzI3OWZiIiwiaWF0IjoxNTU3NDcyMzA5LCJleHAiOjE1NTc1NTg3MDl9.N7kfEdQXqoxF52tPOQ_NM7UbImWNWg8-MbVDzppyPjQ"

interface Api {
    @Headers("Content-Type: application/json")
    @POST("hotels/search")
    fun searchHotelAsync(@Body searchBody: SearchHotelBody): Deferred<SearchHotelReponse>

    @Headers("Content-Type: application/json")
    @POST("flights/search")
    fun searchFlight(@Body searchBody: SearchFlightBody, @Query("page") page: Int): Call<SearchFlightResponse>

    companion object {
        operator fun invoke(retrofit: Retrofit): Api = retrofit.create()
    }
}