package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class Meta(
    @Json(name = "page")
    val page: Int, // 2
    @Json(name = "page_size")
    val pageSize: Int, // 10
    @Json(name = "total_page")
    val totalPage: Int, // 60
    @Json(name = "total_size")
    val totalSize: Int // 597
)