package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class TimeEnd(
    @Json(name = "hour")
    val hour: Int, // 10
    @Json(name = "minute")
    val minute: Int // 50
)