package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class EndLocation(
    @Json(name = "acronym")
    val acronym: String, // HAN
    @Json(name = "country")
    val country: String, // Việt Nam
    @Json(name = "has_airport")
    val hasAirport: Boolean, // true
    @Json(name = "_id")
    val id: String, // 5ccaec2a6a3a046671ed7126
    @Json(name = "image")
    val image: String, // uploads/img_city_ha-noi.jpg
    @Json(name = "name")
    val name: String, // Hà Nội
    @Json(name = "__v")
    val v: Int, // 0
    @Json(name = "zipcode")
    val zipcode: Int // 100000
)