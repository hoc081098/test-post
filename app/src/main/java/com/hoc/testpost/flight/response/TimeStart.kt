package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class TimeStart(
    @Json(name = "hour")
    val hour: Int, // 8
    @Json(name = "minute")
    val minute: Int // 45
)