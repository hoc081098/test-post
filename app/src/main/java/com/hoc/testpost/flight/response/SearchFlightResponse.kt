package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class SearchFlightResponse(
    @Json(name = "data")
    val `data`: Data,
    @Json(name = "meta")
    val meta: Meta,
    @Json(name = "success")
    val success: Boolean // true
)