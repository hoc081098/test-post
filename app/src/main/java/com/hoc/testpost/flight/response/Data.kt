package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class Data(
    @Json(name = "flights")
    val flights: List<Flight>
)