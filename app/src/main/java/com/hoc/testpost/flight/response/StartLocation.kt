package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class StartLocation(
    @Json(name = "acronym")
    val acronym: String, // SGN
    @Json(name = "country")
    val country: String, // Việt Nam
    @Json(name = "has_airport")
    val hasAirport: Boolean, // true
    @Json(name = "_id")
    val id: String, // 5ccaec2a6a3a046671ed712a
    @Json(name = "image")
    val image: String, // uploads/img_city_ho-chi-minh.jpg
    @Json(name = "name")
    val name: String, // TP. Hồ Chí Minh
    @Json(name = "__v")
    val v: Int, // 0
    @Json(name = "zipcode")
    val zipcode: Int // 700000
)