package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class Provider(
    @Json(name = "acronym")
    val acronym: String, // VJ
    @Json(name = "_id")
    val id: String, // 5ccaec2a6a3a046671ed7146
    @Json(name = "name")
    val name: String, // VietJet Air
    @Json(name = "__v")
    val v: Int // 0
)