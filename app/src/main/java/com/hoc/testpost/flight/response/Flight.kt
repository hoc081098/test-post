package com.hoc.testpost.flight.response


import com.squareup.moshi.Json

data class Flight(
    @Json(name = "date_end")
    val dateEnd: String, // 2019-05-29T00:00:00.000Z
    @Json(name = "date_start")
    val dateStart: String, // 2019-05-29T00:00:00.000Z
    @Json(name = "end_location")
    val endLocation: EndLocation,
    @Json(name = "_id")
    val id: String, // 5ccafa276a3a046671edf900
    @Json(name = "price")
    val price: Int, // 67
    @Json(name = "provider")
    val provider: Provider,
    @Json(name = "start_location")
    val startLocation: StartLocation,
    @Json(name = "time_end")
    val timeEnd: TimeEnd,
    @Json(name = "time_start")
    val timeStart: TimeStart
)