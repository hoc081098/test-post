package com.hoc.testpost.flight.body


import com.squareup.moshi.Json

data class SearchFlightBody(
    @Json(name = "cities")
    val cities: List<String>,
    @Json(name = "dates")
    val dates: List<String>
)