package com.hoc.testpost

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.*
import com.hoc.testpost.hotel.body.Location
import com.hoc.testpost.hotel.body.SearchHotelBody
import com.hoc.testpost.hotel.response.Guests
import com.hoc.testpost.hotel.response.Hotel
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class HotelAdapter : ListAdapter<Hotel, HotelAdapter.VH>(object : DiffUtil.ItemCallback<Hotel>() {
    override fun areItemsTheSame(oldItem: Hotel, newItem: Hotel): Boolean = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: Hotel, newItem: Hotel): Boolean = oldItem == newItem
}) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_2, parent, false)
            .let(HotelAdapter::VH)
    }

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind(getItem(position))

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val text1 = itemView.findViewById<TextView>(android.R.id.text1)!!
        private val text2 = itemView.findViewById<TextView>(android.R.id.text2)!!

        fun bind(item: Hotel) {
            text1.text = item.name
            text2.text = "${item.description}-${item.address}"
        }
    }
}

class MainActivity : AppCompatActivity(), CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Main + job
    private val hotelAdapter = HotelAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recycler.run {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = hotelAdapter
            val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            addItemDecoration(dividerItemDecoration)
        }

        search()

        button_refresh.setOnClickListener { search() }
    }

    private fun search() {
        launch {
            progress_bar.visibility = View.VISIBLE

            runCatching {
                val searchHotelResponse = withContext(Dispatchers.IO) {
                    val searchBody = SearchHotelBody(
                        location = Location(
                            address = "Hồ gươm",
                            coordinates = listOf(
                                105.850176,
                                21.0287747
                            )
                        ),
                        dates = listOf(
                            "26/04/2019",
                            "9/05/2019"
                        ),
                        prices = listOf(
                            100,
                            500
                        ),
                        guests = Guests(
                            rooms = 1,
                            peoples = 1
                        )
                    )
                    DepLocator.api.searchHotelAsync(searchBody).await()
                }

                hotelAdapter.submitList(searchHotelResponse.data.hotels)

                progress_bar.visibility = View.INVISIBLE
                Toast.makeText(this@MainActivity, "Search done", Toast.LENGTH_SHORT).show()
            }.onFailure {
                progress_bar.visibility = View.INVISIBLE
                Toast.makeText(this@MainActivity, it.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}
