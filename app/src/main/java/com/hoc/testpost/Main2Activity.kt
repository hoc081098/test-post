package com.hoc.testpost

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.*
import com.hoc.testpost.flight.body.SearchFlightBody
import com.hoc.testpost.flight.response.Flight
import com.hoc.testpost.flight.response.SearchFlightResponse
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_error.view.*
import kotlinx.android.synthetic.main.item_flight_vertical.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

sealed class FlightListItem {
    object LoadingItem : FlightListItem()
    data class FlightItem(val flight: Flight) : FlightListItem()
    data class ErrorItem(val throwable: Throwable?) : FlightListItem()
}

class FlightAdapter(
    private val retry: () -> Unit
) : ListAdapter<FlightListItem, FlightAdapter.VH>(object : DiffUtil.ItemCallback<FlightListItem>() {
    override fun areItemsTheSame(oldItem: FlightListItem, newItem: FlightListItem): Boolean {
        return when {
            newItem is FlightListItem.FlightItem && oldItem is FlightListItem.FlightItem -> newItem.flight.id == oldItem.flight.id
            newItem is FlightListItem.LoadingItem && oldItem is FlightListItem.LoadingItem -> true
            newItem is FlightListItem.ErrorItem && oldItem is FlightListItem.ErrorItem -> newItem.throwable == oldItem.throwable
            else -> newItem == oldItem
        }
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: FlightListItem, newItem: FlightListItem): Boolean = oldItem == newItem
}) {
    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            FlightListItem.LoadingItem -> R.layout.item_loading
            is FlightListItem.FlightItem -> R.layout.item_flight_vertical
            is FlightListItem.ErrorItem -> R.layout.item_error
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return LayoutInflater.from(parent.context).inflate(viewType, parent, false).let {
            when (viewType) {
                R.layout.item_loading -> LoadingVH(it)
                R.layout.item_flight_vertical -> FlightVH(it)
                R.layout.item_error -> ErrorVH(it, retry)
                else -> error("Unknown viewType=$viewType")
            }
        }
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(getItem(position))
    }

    abstract class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: FlightListItem)
    }

    class FlightVH(itemView: View) : VH(itemView) {
        private val tvAirline = itemView.tvAirline!!
        private val tvDepartureTime = itemView.tvDepartureTime!!
        private val tvArrivalTime = itemView.tvArrivalTime!!
        private val tvPrices = itemView.tvPrices!!
        private val tv_item_start_location = itemView.tv_item_start_location!!
        private val tv_item_destination = itemView.tv_item_destination!!
        private val tvDurationFlight = itemView.tvDurationFlight!!
        private val tvTypeFlight = itemView.tvTypeFlight!!

        override fun bind(item: FlightListItem) {
            if (item !is FlightListItem.FlightItem) {
                return
            }
            val flight = item.flight
            tvAirline.text =
                "${flight.startLocation.name}-${flight.startLocation.country} -> ${flight.endLocation.name}-${flight.endLocation.country}"
        }
    }

    class ErrorVH(itemView: View, retry: () -> Unit) : VH(itemView) {
        private val textError = itemView.text_error!!
        private val buttonRetry = itemView.button_retry!!

        init {
            buttonRetry.setOnClickListener {
                retry()
            }
        }

        override fun bind(item: FlightListItem) {
            if (item !is FlightListItem.ErrorItem) {
                return
            }
            textError.text = item.throwable.toString()
        }
    }

    class LoadingVH(itemView: View) : VH(itemView) {
        override fun bind(item: FlightListItem) {
        }
    }
}

class Main2Activity : AppCompatActivity() {

    private val flightAdapter = FlightAdapter(::retry)

    private fun retry() {
        loadNextPage()
    }

    private var page = 1
    private var isLoading = false
    private var items = emptyList<FlightListItem>()

    private val searchFlightBody = SearchFlightBody(
        cities = listOf("SGN", "HAN"),
        dates = listOf("29/05/2019")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        recycler.run {
            setHasFixedSize(true)
            val linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            adapter = flightAdapter
            val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            addItemDecoration(dividerItemDecoration)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (linearLayoutManager.findLastVisibleItemPosition() + THRESHOLD >= linearLayoutManager.itemCount
                        && !isLoading
                        && items.lastOrNull() !is FlightListItem.ErrorItem
                    ) {
                        loadNextPage()
                    }
                }
            })
        }

        loadNextPage()
    }

    private fun loadNextPage() {
        isLoading = true

        // add loading item
        items = items.dropLastWhile { it !is FlightListItem.FlightItem } + FlightListItem.LoadingItem
        flightAdapter.submitList(items)

        DepLocator.api.searchFlight(
            searchBody = searchFlightBody,
            page = page
        ).enqueue(object : Callback<SearchFlightResponse?> {
            override fun onFailure(call: Call<SearchFlightResponse?>, t: Throwable) {
                Toast.makeText(this@Main2Activity, "Failure: $t", Toast.LENGTH_SHORT)
                    .show()

                // remove loading item and error item, then add error item
                val newList =
                    items.dropLastWhile { it !is FlightListItem.FlightItem } + FlightListItem.ErrorItem(t)
                flightAdapter.submitList(newList.also { items = newList })
                isLoading = false
            }

            override fun onResponse(call: Call<SearchFlightResponse?>, response: Response<SearchFlightResponse?>) {
                when {
                    response.isSuccessful -> {
                        val flightResponse = response.body() ?: return

                        // remove loading item and error item, then append new list
                        val newList = items.dropLastWhile { it !is FlightListItem.FlightItem } +
                                flightResponse.data.flights.map { FlightListItem.FlightItem(it) }
                        flightAdapter.submitList(newList.also { items = newList })
                    }
                    else -> {
                        Toast.makeText(this@Main2Activity, "Error body: ${response.errorBody()}", Toast.LENGTH_SHORT)
                            .show()

                        // remove loading item and error item, then add error item
                        val newList =
                            items.dropLastWhile { it !is FlightListItem.FlightItem } + FlightListItem.ErrorItem(null)
                        flightAdapter.submitList(newList.also { items = newList })
                    }
                }
                page++
                isLoading = false
            }
        })
    }

    companion object {
        private const val THRESHOLD = 1
    }
}